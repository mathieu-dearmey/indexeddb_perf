function roughSizeOfObject(object) {
  const objectString = JSON.stringify(object);
  const encoder = new TextEncoder();
  const bytes = encoder.encode(objectString);
  const megabytes = bytes.length / (1024 * 1024);
  return megabytes.toFixed(2);
}

function getCollectionAmount(data) {
  return data.map((item) => item.data).flat().length;
}

document.addEventListener("DOMContentLoaded", function () {
  const dbName = "singleDb";
  const collectionNames = Array(20)
    .fill(null)
    .map((_, index) => `collection${index + 1}`);

  const populateButton = document.getElementById("populateButton");
  const readFromWorkerButton = document.getElementById("readFromWorkerButton");
  const readFromMainThreadButton = document.getElementById(
    "readFromMainThreadButton",
  );
  const statusMessage = document.getElementById("statusMessage");
  const container = document.getElementById("container");

  populateButton.addEventListener("click", async function () {
    statusMessage.textContent = "Populating...";
    // Assume data.json is a JSON file with an array of objects
    const response = await fetch("data.json");
    const data = await response.json();

    const request = indexedDB.open(dbName, 1);

    request.onupgradeneeded = function (event) {
      const db = event.target.result;
      collectionNames.forEach((collectionName) => {
        const objectStore = db.createObjectStore(collectionName, {
          keyPath: "_id",
        });

        // Populate data from JSON
        data.forEach((item) => {
          objectStore.add(item);
        });
      });
    };

    request.onsuccess = function (event) {
      const db = event.target.result;
      db.close();
      statusMessage.textContent = "Populating complete.";
    };
  });

  readFromWorkerButton.addEventListener("click", function () {
    statusMessage.textContent = "Reading DB through worker...";
    const start = Date.now();
    const worker = new Worker("readWorker.js");
    worker.postMessage({ dbName, collectionNames });

    worker.onmessage = function (event) {
      const dataFromDB = event.data;
      const end = Date.now();
      console.log("datafromDb", dataFromDB);
      statusMessage.textContent = "";
      const newParagraph = document.createElement("p");
      newParagraph.textContent = `${getCollectionAmount(dataFromDB)} objects retrieved from DB in ${end - start}ms (WORKER)`;
      container.appendChild(newParagraph);
    };
  });

  readFromMainThreadButton.addEventListener("click", function () {
    statusMessage.textContent = "Reading DB (main thread)...";
    // Read data from IndexedDB (you can modify this part based on your needs)
    const dataFromDB = [];

    const start = Date.now();
    const request = indexedDB.open(dbName, 1);

    request.onsuccess = function (event) {
      const db = event.target.result;
      const transaction = db.transaction(collectionNames, "readonly");
      collectionNames.forEach((collectionName) => {
        const objectStore = transaction.objectStore(collectionName);
        const cursorRequest = objectStore.openCursor();

        cursorRequest.onsuccess = function (cursorEvent) {
          const cursor = cursorEvent.target.result;
          if (cursor) {
            dataFromDB.push(cursor.value);
            cursor.continue();
          }
        };
      });

      transaction.oncomplete = function () {
        const end = Date.now();
        db.close();
        statusMessage.textContent = " ";
        const newParagraph = document.createElement("p");

        newParagraph.textContent = `${getCollectionAmount(dataFromDB)} objects retrieved from DB in ${end - start}ms (MAIN THREAD)`;
        container.appendChild(newParagraph);
      };
    };
  });
});
