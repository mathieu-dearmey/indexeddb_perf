onmessage = function (event) {
  const { dbName, collectionNames } = event.data;
  const dataFromDB = [];

  const request = indexedDB.open(dbName, 1);

  request.onsuccess = function (event) {
    const db = event.target.result;
    const promises = [];

    collectionNames.forEach((collectionName) => {
      const promise = new Promise((resolve, reject) => {
        const transaction = db.transaction([collectionName], "readonly");
        const objectStore = transaction.objectStore(collectionName);
        const data = [];

        const cursorRequest = objectStore.openCursor();

        cursorRequest.onsuccess = function (cursorEvent) {
          const cursor = cursorEvent.target.result;
          if (cursor) {
            console.log(typeof cursor.value);
            data.push(cursor.value);
            cursor.continue();
          } else {
            resolve({ collectionName, data });
          }
        };

        cursorRequest.onerror = function (error) {
          reject(error);
        };
      });

      promises.push(promise);
    });

    Promise.all(promises)
      .then((results) => {
        db.close();
        postMessage(results);
      })
      .catch((error) => {
        db.close();
        postMessage({ error: error });
      });
  };
};
