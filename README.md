Install `http-server` (you can install the npm package globally) to serve the application on localhost.

Once installed, open the terminal at the level of **multipledatabases** or **singledatabase**.

Run the command:

`http-server -c-1`


You can provide a port number by using the `-p` argument (e.g., `-p 8081`) if you want to run the two sites at the same time.

You can open your localhost by using one of the provided URLs (e.g., http://192.168.1.60:8080/).

Once served, open localhost:
- Click on the "Populate" button to write the data to IndexedDB.
- Use the "read" buttons to measure DB read times (via worker on the main thread).

In your browser devtools, you can use `performance > CPU slowdown` to simulate a slower machine.

In `main.js`, you can change the number of collections/DBs you want by updating the `dbNames` or `collectionNames` Array length.
