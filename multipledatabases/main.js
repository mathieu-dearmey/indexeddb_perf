function roughSizeOfObject(object) {
  const objectString = JSON.stringify(object);
  const encoder = new TextEncoder();
  const bytes = encoder.encode(objectString);
  const megabytes = bytes.length / (1024 * 1024);
  return megabytes.toFixed(2);
}

function getCollectionAmount(data) {
  return data.flat().length;
}

document.addEventListener("DOMContentLoaded", function () {
  const databaseNames = Array(20)
    .fill(null)
    .map((_, index) => `db${index + 1}`);

  const populateButton = document.getElementById("populateButton");
  const readFromWorkerButton = document.getElementById("readFromWorkerButton");
  const readFromMainThreadButton = document.getElementById(
    "readFromMainThreadButton",
  );
  const statusMessage = document.getElementById("statusMessage");
  const container = document.getElementById("container");

  populateButton.addEventListener("click", async function () {
    statusMessage.textContent = "Populating...";
    // Assume data.json is a JSON file with an array of objects
    const response = await fetch("data.json");
    const data = await response.json();

    databaseNames.forEach((dbName, index) => {
      const request = indexedDB.open(dbName, 1);

      request.onupgradeneeded = function (event) {
        const db = event.target.result;
        const objectStore = db.createObjectStore(`collection${index + 1}`, {
          keyPath: "_id",
        });

        // Populate data from JSON
        data.forEach((item) => {
          objectStore.add(item);
        });
      };

      request.onsuccess = function (event) {
        const db = event.target.result;
        db.close();
        statusMessage.textContent = "Populating complete.";
      };
    });
  });

  readFromWorkerButton.addEventListener("click", function () {
    statusMessage.textContent = "Reading DBS through workers...";
    const workers = [];

    const dataFromDB = [];
    const start = Date.now();
    databaseNames.forEach((dbName, index) => {
      const worker = new Worker("readWorker.js");
      worker.postMessage({
        dbName,
        collectionNames: [`collection${index + 1}`],
      });

      worker.onmessage = function (event) {
        dataFromDB.push(event.data);

        workers.push(worker);

        if (workers.length === databaseNames.length) {
          const end = Date.now();
          // All workers have completed
          workers.forEach((worker) => worker.terminate());
          statusMessage.textContent = "";
          const newParagraph = document.createElement("p");
          newParagraph.textContent = `${dataFromDB.flat().length} objects retrieved from DB in ${end - start}ms (WORKERS)`;
          container.appendChild(newParagraph);
        }
      };
    });
  });

  readFromMainThreadButton.addEventListener("click", function () {
    statusMessage.textContent = "Reading DBS (main thread)...";
    const start = Date.now();
    const promises = databaseNames.map((dbName, index) => {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(dbName, 1);

        request.onsuccess = function (event) {
          const db = event.target.result;
          const transaction = db.transaction(
            [`collection${index + 1}`],
            "readonly",
          );
          const objectStore = transaction.objectStore(`collection${index + 1}`);
          const cursorRequest = objectStore.openCursor();

          const dataFromDB = [];

          cursorRequest.onsuccess = function (cursorEvent) {
            const cursor = cursorEvent.target.result;
            if (cursor) {
              dataFromDB.push(cursor.value);
              cursor.continue();
            } else {
              resolve({ dbName, data: dataFromDB });
            }
          };

          cursorRequest.onerror = function (error) {
            reject(error);
          };

          transaction.oncomplete = function () {
            db.close();
          };
        };

        request.onerror = function (error) {
          reject(error);
        };
      });
    });

    Promise.all(promises)
      .then((results) => {
        const end = Date.now();
        statusMessage.textContent = " ";

        const newParagraph = document.createElement("p");
        newParagraph.textContent = `${results.map((item) => item.data).flat().length} objects retrieved from DB in ${end - start}ms (MAIN THREAD)`;
        container.appendChild(newParagraph);
      })
      .catch((error) => {
        console.error("Error reading from IndexedDB:", error);
      });
  });
});
