onmessage = function (event) {
  const { dbName, collectionNames } = event.data;
  const dataFromDB = [];

  const request = indexedDB.open(dbName, 1);

  request.onsuccess = function (event) {
    const db = event.target.result;

    collectionNames.forEach((collectionName) => {
      const transaction = db.transaction([collectionName], "readonly");
      const objectStore = transaction.objectStore(collectionName);
      const cursorRequest = objectStore.openCursor();

      cursorRequest.onsuccess = function (cursorEvent) {
        const cursor = cursorEvent.target.result;
        if (cursor) {
          dataFromDB.push(cursor.value);
          cursor.continue();
        }
      };

      transaction.oncomplete = function () {
        postMessage(dataFromDB);
      };
    });

    db.close();
  };
};
